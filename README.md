# Created For Both Developers and Users

>**Reducer by REDUX**. The apps controller build based on Redux Reducer concept.
All state function are splits into switch case (create, park, leave, find, status) all standing inside 1 terminal function class. It's tidy, easy to maintenance and enhance. Core function are saved into one package and possible to moved as standalone library (but now allowed by the rules).
Its used run time memory to store data **static declaration**

# Parking Lot Application (CLI)

It's here now, parking lot application that enable to maintain business process on parking lot space with capacity management and status monitoring.
It's JAVA 1.8 based, so it's build using current function that help to reduce processing time.

CHEERS....

## Getting Started

JAVA 1.8 NEEDED, PLEASE ACCESSS [HERE FOR JDK](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html) and [HERE FOR JRE](http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html) TO INSTALL JAVA. It's build under maven so make sure mvn are able to executed. PLEASE ACCESS [HERE FOR MAVEN](https://maven.apache.org/install.html). If you are developer, please use IDE to open the project [EXAMPLE: Intellij](https://www.jetbrains.com/idea/):

to build and test
```
mvn clean install
```

### Prerequisites

JAVA 1.8

```
(http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
(http://www.oracle.com/technetwork/java/javase/downloads/jre8-downloads-2133155.html)
```

Test Tools (JUnit):
```
<dependencies>
	<dependency>
		<groupId>junit</groupId>
		<artifactId>junit</artifactId>
		<version>4.12</version>
	</dependency>
</dependencies>
```

### Installing

By Maven **plus run the test case**

```
Mvn clean install
```

Get Source-Code by GIT

```
[Private Repository](git clone https://yoedi_har_id@bitbucket.org/yoedi_har_id/parking-lot-2.git)
```

## Running the tests

```
Mvn test
```

### Break down into end to end tests

#### Service Test Case:
* Test functionality (unit test)

#### And coding style tests:

* It's TDD, test case first then code follow up :)
* Function to return error (nil on OK state)

```java
@Test
public void isEvenTest(){
	//use lib (Read Prerequisites)
	Assert.assertEqual(false,isEven(1));
	Assert.assertEqual(true,isEven(2));
}

public boolean isEven(int input) {
	//Close Guard
	if (input%2 != 0) {
		return true
	}
	return false
}
```

## RUNNING (Inside Target Folder)

Windows:
```
java -jar parkinglot-1.0.0-LIVE.jar
```

Linux:
```
java -jar parkinglot-1.0.0-LIVE.jar
```

## DEPLOYMENT SCRIPT (Inside Target Folder)

### Please make sure the files are executeable (Linux)

Windows:
```
parking_lot.bat
```

Linux:
```
parking_lot.sh
```

## Contributing

Please read [CONTRIBUTING.md](https://gist.github.com/PurpleBooth/b24679402957c63ec426) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

My first day
V 1.0.0

## Authors

* **Yoedi Hariadi Kurniawan** - *Passionate Software Engineer*

## License



## Acknowledgments
