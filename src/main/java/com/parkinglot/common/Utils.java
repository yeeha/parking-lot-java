package com.parkinglot.common;

import com.parkinglot.model.Car;
import com.parkinglot.state.ResponseCode;
import com.parkinglot.state.StateFunction;
import com.parkinglot.state.StateFunction;

import java.io.File;
import java.util.HashMap;
import java.util.Scanner;

/**
 * Created by Yody Hariadi on 9/5/2017.
 */
public interface Utils {

    Scanner scanner = new Scanner(System.in);

    static Scanner inputFile(String filename){
        try{
            File file = new File(filename);
            Scanner input = new Scanner(file);
            return input;
        }catch (Exception e){
            return null;
        }
    }

    static String trimComma(String str){
        final int len = str.length();
        return len > 0 && str.charAt(len - 1) == ',' ?
                str.substring(0, len - 1) :
                str;
    }

    static String printStatus(HashMap<Integer,Car> lists, StateFunction stateFunction){
        StringBuffer out = new StringBuffer();
        boolean header = true;
        for (Integer key: lists.keySet()){
            Car temp = lists.get(key);
            switch (stateFunction){
                case REGISTRATION_NUMBERS_FOR_CARS_WITH_COLOUR:
                    out.append(temp.getRegistrationNumber()+", ");
                    break;
                case SLOT_NUMBERS_FOR_CARS_WITH_COLOUR:
                    out.append(key+", ");
                    break;
                case SLOT_NUMBER_FOR_REGISTRATION_NUMBER:
                    out.append(key);
                    break;
                case STATUS:
                    if (header){
                        out.append(String.format("%-8s %-20s %-10s\n","Slot No.","Registration No","Colour"));
                        header=false;
                    }
                    out.append(String.format("%-8d %-20s %-10s\n",key,temp.getRegistrationNumber(),temp.getColour()));
                    break;
                default:
                    out.append(ResponseCode.BAD_PARAMETER.toString());
                    break;
            }
        }
        return trimComma(out.toString().trim());
    }
}
