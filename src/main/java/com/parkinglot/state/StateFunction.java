package com.parkinglot.state;

/**
 * Created by Yody Hariadi on 9/5/2017.
 */
public enum StateFunction {
    CREATE_PARKING_LOT,
    PARK,
    LEAVE,
    STATUS,
    REGISTRATION_NUMBERS_FOR_CARS_WITH_COLOUR,
    SLOT_NUMBERS_FOR_CARS_WITH_COLOUR,
    SLOT_NUMBER_FOR_REGISTRATION_NUMBER;
}
