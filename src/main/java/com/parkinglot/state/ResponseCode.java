package com.parkinglot.state;

/**
 * Created by Yody Hariadi on 9/5/2017.
 */
public enum ResponseCode {
    //positive case
    SUCCESS_CREATE("Created a parking lot with %d slots"),
    SUCCESS_PARK("Allocated slot number: %d"),
    SUCCESS_LEAVE("Slot number %d is free"),

    //negative case
    PARKING_LOT_IS_FULL("Sorry, parking lot is full"),
    BAD_PARAMETER("Bad parameter"),
    PARKING_LOT_NOT_INITIALIZED("Parking lot not initialized"),
    NO_CAR_PARKED("No car parked"),
    PARKING_LOT_ALREADY_INITIALIZED("Parking lot already initialized"),
    NOT_FOUND("Not found");

    private final String text;
    ResponseCode(final String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return text;
    }


    public String toString(int param) {
        return String.format(text,param);
    }
}
