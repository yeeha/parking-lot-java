package com.parkinglot.service;

import com.parkinglot.state.ResponseCode;
import com.parkinglot.state.StateFunction;
import com.parkinglot.common.Utils;
import com.parkinglot.model.Car;
import com.parkinglot.state.StateFunction;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

/**
 * Created by Yody Hariadi on 9/5/2017.
 */
public class ParkingLotService {
    private static int MAX = 0;
    private static HashMap<Integer,Car> parkingLot;
    private static List<Integer> emptySlots;

    public static void setMAX(int MAX) {
        ParkingLotService.MAX = MAX;
    }

    public static void setParkingLot(HashMap<Integer, Car> parkingLot) {
        ParkingLotService.parkingLot = parkingLot;
    }

    public static void setEmptySlots(List<Integer> emptySlots) {
        ParkingLotService.emptySlots = emptySlots;
    }

    public static String closeGuard(StateFunction state){
        String out=null;
        switch (state){
            case CREATE_PARKING_LOT:
                if(parkingLot !=null){
                    return ResponseCode.PARKING_LOT_ALREADY_INITIALIZED.toString();
                }
                break;
            case PARK:
                if(parkingLot ==null){
                    return ResponseCode.PARKING_LOT_NOT_INITIALIZED.toString();
                }
                if(emptySlots.size()==0){
                    return ResponseCode.PARKING_LOT_IS_FULL.toString();
                }
                break;
            case LEAVE:
            case STATUS:
            case REGISTRATION_NUMBERS_FOR_CARS_WITH_COLOUR:
            case SLOT_NUMBER_FOR_REGISTRATION_NUMBER:
            case SLOT_NUMBERS_FOR_CARS_WITH_COLOUR:
                if(parkingLot ==null){
                    return ResponseCode.PARKING_LOT_NOT_INITIALIZED.toString();
                }
                if(emptySlots.size()==MAX){
                    return ResponseCode.NO_CAR_PARKED.toString();
                }
                break;
        }
        return out;
    }

    public static String create(int slots) {
        if(slots==0){
            return ResponseCode.BAD_PARAMETER.toString();
        }
        String closeGuard = closeGuard(StateFunction.CREATE_PARKING_LOT);
        if(closeGuard!=null){
            return closeGuard;
        }

        MAX = slots;
        parkingLot = new HashMap<>();
        emptySlots = IntStream.of(IntStream.rangeClosed(1,slots).toArray()).boxed().collect(Collectors.toList());
        return ResponseCode.SUCCESS_CREATE.toString(slots);
    }

    public static String fillSlot(Car car) {
        if(car == null){
            return ResponseCode.BAD_PARAMETER.toString();
        }
        String closeGuard = closeGuard(StateFunction.PARK);
        if(closeGuard!=null){
            return closeGuard;
        }

        int slot = emptySlots.get(0);
        emptySlots.remove(0);
        parkingLot.put(slot,car);
        return ResponseCode.SUCCESS_PARK.toString(slot);
    }

    public static String freeSlot(int slot) {
        if(slot==0){
            return ResponseCode.BAD_PARAMETER.toString();
        }
        String closeGuard = closeGuard(StateFunction.LEAVE);
        if(closeGuard!=null){
            return closeGuard;
        }
        if(slot>MAX){
            return ResponseCode.BAD_PARAMETER.toString();
        }

        parkingLot.remove(slot);
        emptySlots.add(slot);
        Collections.sort(emptySlots);
        return ResponseCode.SUCCESS_LEAVE.toString(slot);
    }

    public static String status(StateFunction stateFunction) {
        String closeGuard = closeGuard(StateFunction.STATUS);
        if(closeGuard!=null){
            return closeGuard;
        }
        return Utils.printStatus(parkingLot,stateFunction);
    }

    public static String find(Car term, boolean isArray, StateFunction stateFunction) {
        if(term==null){
            return ResponseCode.BAD_PARAMETER.toString();
        }
        String closeGuard = closeGuard(stateFunction);
        if(closeGuard!=null){
            return closeGuard;
        }
        HashMap<Integer,Car> founded = new HashMap<>();
        for (Integer name: parkingLot.keySet()){
            Car temp = parkingLot.get(name);
            if(term.getColour()==null){
                if(temp.getRegistrationNumber().equalsIgnoreCase(term.getRegistrationNumber())){
                    founded.put(name,temp);
                    if(isArray==false)
                        break;
                }
            }
            else if(term.getRegistrationNumber()==null){
                if(temp.getColour().equalsIgnoreCase(term.getColour())){
                    founded.put(name,temp);
                }
            }
        }
        String foundStr;
        return (foundStr=Utils.printStatus(founded,stateFunction)).isEmpty()?ResponseCode.NOT_FOUND.toString():foundStr;
    }
}
