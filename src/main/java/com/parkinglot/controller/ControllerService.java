package com.parkinglot.controller;

import com.parkinglot.model.Car;
import com.parkinglot.service.ParkingLotService;
import com.parkinglot.state.StateFunction;
import com.parkinglot.state.StateFunction;

import java.util.Scanner;

/**
 * Created by Yody Hariadi on 9/5/2017.
 */
public interface ControllerService {
    static String switchFunctionState(String command, Scanner scanner){
        String response = "";
        try {
            switch (StateFunction.valueOf(command.toUpperCase())) {
                case CREATE_PARKING_LOT:
                    response = ParkingLotService.create(scanner.nextInt());
                    break;
                case PARK:
                    response = ParkingLotService.fillSlot(new Car(scanner.next(), scanner.next()));
                    break;
                case LEAVE:
                    response = ParkingLotService.freeSlot(scanner.nextInt());
                    break;
                case STATUS:
                    response = ParkingLotService.status(StateFunction.STATUS);
                    break;
                case REGISTRATION_NUMBERS_FOR_CARS_WITH_COLOUR:
                    response = ParkingLotService.find(new Car(null,scanner.next()),false,StateFunction.REGISTRATION_NUMBERS_FOR_CARS_WITH_COLOUR);
                    break;
                case SLOT_NUMBERS_FOR_CARS_WITH_COLOUR:
                    response = ParkingLotService.find(new Car(null,scanner.next()),false,StateFunction.SLOT_NUMBERS_FOR_CARS_WITH_COLOUR);
                    break;
                case SLOT_NUMBER_FOR_REGISTRATION_NUMBER:
                    response = ParkingLotService.find(new Car(scanner.next(),null),true,StateFunction.SLOT_NUMBER_FOR_REGISTRATION_NUMBER);
                    break;
                default:
                    response = "Bad Command";
                    break;
            }
        }catch (Exception e){
            response = "Bad Request Parameters";
        }
        return response;
    }
}
