package com.parkinglot;

import com.parkinglot.common.Utils;
import com.parkinglot.controller.ControllerService;
import com.parkinglot.common.Utils;
import com.parkinglot.controller.ControllerService;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {
        if(args.length!=0){
            Scanner scanner = Utils.inputFile(args[0]);
            while (scanner.hasNext()) {
                System.out.println(ControllerService.switchFunctionState(scanner.next(),scanner));
            }
            scanner.close();
        }
        while (true)
            System.out.println(ControllerService.switchFunctionState(Utils.scanner.next(),Utils.scanner));
    }
}
