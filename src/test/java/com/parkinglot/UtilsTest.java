package com.parkinglot;

import com.parkinglot.common.Utils;
import com.parkinglot.model.Car;
import org.junit.Assert;
import org.junit.Test;
import com.parkinglot.state.StateFunction;

import java.util.HashMap;

/**
 * Created by Yody Hariadi on 9/5/2017.
 */
public class UtilsTest{
    @Test
    public void trimCommaTest(){
        Assert.assertEquals("asd", Utils.trimComma("asd,"));
        Assert.assertEquals("asd",Utils.trimComma("asd"));
        Assert.assertEquals("asd,qwe",Utils.trimComma("asd,qwe"));
        Assert.assertEquals("asd,qwe",Utils.trimComma("asd,qwe,"));
    }

    @Test
    public void printStatusTest(){
        HashMap<Integer, Car> temp = new HashMap<>();
        temp.put(1,new Car("KA-01-HH-1234","White"));
        temp.put(2,new Car("KA-01-HH-9999","White"));
        temp.put(4,new Car("KA-01-P-333","White"));
        Assert.assertEquals("KA-01-HH-1234, KA-01-HH-9999, KA-01-P-333",Utils.printStatus(temp, StateFunction.REGISTRATION_NUMBERS_FOR_CARS_WITH_COLOUR));
        Assert.assertEquals("1, 2, 4",Utils.printStatus(temp, StateFunction.SLOT_NUMBERS_FOR_CARS_WITH_COLOUR));
        Assert.assertEquals("124",Utils.printStatus(temp,StateFunction.SLOT_NUMBER_FOR_REGISTRATION_NUMBER));
    }
}
