package com.parkinglot;

import com.parkinglot.model.Car;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import com.parkinglot.service.ParkingLotService;
import com.parkinglot.state.ResponseCode;
import com.parkinglot.state.StateFunction;

/**
 * Created by Yody Hariadi on 9/6/2017.
 */
public class ParkingLotServiceTest {

    @Before
    public void init() {
        ParkingLotService.setMAX(0);
        ParkingLotService.setParkingLot(null);
        ParkingLotService.setEmptySlots(null);
    }

    @Test
    public void createTest() {
        Assert.assertEquals(ResponseCode.BAD_PARAMETER.toString(), ParkingLotService.create(0));
        Assert.assertEquals(ResponseCode.SUCCESS_CREATE.toString(6), ParkingLotService.create(6));
        Assert.assertEquals(ResponseCode.PARKING_LOT_ALREADY_INITIALIZED.toString(), ParkingLotService.create(6));
    }

    @Test
    public void fillSlotTest() {
        Assert.assertEquals(ResponseCode.PARKING_LOT_NOT_INITIALIZED.toString(), ParkingLotService.fillSlot(new Car("a", "b")));
        ParkingLotService.create(1);
        Assert.assertEquals(ResponseCode.BAD_PARAMETER.toString(), ParkingLotService.fillSlot(null));
        Assert.assertEquals(ResponseCode.SUCCESS_PARK.toString(1), ParkingLotService.fillSlot(new Car("a", "b")));
        Assert.assertEquals(ResponseCode.PARKING_LOT_IS_FULL.toString(), ParkingLotService.fillSlot(new Car("a", "b")));
    }

    @Test
    public void freeSlotTest() {
        Assert.assertEquals(ResponseCode.BAD_PARAMETER.toString(), ParkingLotService.freeSlot(0));
        Assert.assertEquals(ResponseCode.PARKING_LOT_NOT_INITIALIZED.toString(), ParkingLotService.freeSlot(1));
        ParkingLotService.create(1);
        Assert.assertEquals(ResponseCode.NO_CAR_PARKED.toString(), ParkingLotService.freeSlot(1));
        ParkingLotService.fillSlot(new Car("a", "b"));
        Assert.assertEquals(ResponseCode.SUCCESS_LEAVE.toString(1), ParkingLotService.freeSlot(1));
    }

    @Test
    public void statusTest() {
        Assert.assertEquals(ResponseCode.PARKING_LOT_NOT_INITIALIZED.toString(), ParkingLotService.status(StateFunction.STATUS));
        ParkingLotService.create(1);
        Assert.assertEquals(ResponseCode.NO_CAR_PARKED.toString(), ParkingLotService.status(StateFunction.STATUS));
        ParkingLotService.fillSlot(new Car("a", "b"));
        Assert.assertEquals("SlotNo.RegistrationNoColour1ab", ParkingLotService.status(StateFunction.STATUS).replace(" ", "").replace("\n", ""));
    }

    @Test
    public void findTest() {
        Assert.assertEquals(ResponseCode.BAD_PARAMETER.toString(), ParkingLotService.find(null, false, StateFunction.SLOT_NUMBER_FOR_REGISTRATION_NUMBER));
        Assert.assertEquals(ResponseCode.PARKING_LOT_NOT_INITIALIZED.toString(), ParkingLotService.find(new Car("a","b"), false, StateFunction.REGISTRATION_NUMBERS_FOR_CARS_WITH_COLOUR));
        Assert.assertEquals(ResponseCode.PARKING_LOT_NOT_INITIALIZED.toString(), ParkingLotService.find(new Car("a","b"), false, StateFunction.SLOT_NUMBERS_FOR_CARS_WITH_COLOUR));
        Assert.assertEquals(ResponseCode.PARKING_LOT_NOT_INITIALIZED.toString(), ParkingLotService.find(new Car("a","b"), false, StateFunction.SLOT_NUMBER_FOR_REGISTRATION_NUMBER));
        ParkingLotService.create(3);
        Assert.assertEquals(ResponseCode.NO_CAR_PARKED.toString(), ParkingLotService.find(new Car("a","b"), false, StateFunction.REGISTRATION_NUMBERS_FOR_CARS_WITH_COLOUR));
        Assert.assertEquals(ResponseCode.NO_CAR_PARKED.toString(), ParkingLotService.find(new Car("a","b"), false, StateFunction.SLOT_NUMBERS_FOR_CARS_WITH_COLOUR));
        Assert.assertEquals(ResponseCode.NO_CAR_PARKED.toString(), ParkingLotService.find(new Car("a","b"), false, StateFunction.SLOT_NUMBER_FOR_REGISTRATION_NUMBER));
        ParkingLotService.fillSlot(new Car("test001", "red"));
        ParkingLotService.fillSlot(new Car("test002", "green"));
        ParkingLotService.fillSlot(new Car("test003", "blue"));
        Assert.assertEquals("test001", ParkingLotService.find(new Car(null, "red"), false, StateFunction.REGISTRATION_NUMBERS_FOR_CARS_WITH_COLOUR));
        Assert.assertEquals("2", ParkingLotService.find(new Car(null, "green"), false, StateFunction.SLOT_NUMBERS_FOR_CARS_WITH_COLOUR));
        Assert.assertEquals("3", ParkingLotService.find(new Car("test003", null), true, StateFunction.SLOT_NUMBER_FOR_REGISTRATION_NUMBER));
        Assert.assertEquals(ResponseCode.NOT_FOUND.toString(), ParkingLotService.find(new Car(null, "red."), false, StateFunction.REGISTRATION_NUMBERS_FOR_CARS_WITH_COLOUR));
        Assert.assertEquals(ResponseCode.NOT_FOUND.toString(), ParkingLotService.find(new Car(null, "green."), false, StateFunction.SLOT_NUMBERS_FOR_CARS_WITH_COLOUR));
        Assert.assertEquals(ResponseCode.NOT_FOUND.toString(), ParkingLotService.find(new Car("test003.", null), true, StateFunction.SLOT_NUMBER_FOR_REGISTRATION_NUMBER));
    }
}
